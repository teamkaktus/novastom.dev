<?php echo $header; ?>
<div class="container">
  <div class="breadcrumb">
    <ul class="breadcrumb-list">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
    <?php echo $edit; ?>
   <!-- <?php echo $address; ?> -->
    <?php echo $order; ?>
</div>
<?php echo $footer; ?>
