
<div class="container">
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>

    <div class="profile">
        <div class="profile-title">Ваши адреса доставки</div>
        <ul class="profile-list">
            <?php $number = 1; ?>
            <?php foreach ($addresses as $result) { ?>
            <li class="profile-list__item">
                <div class="profile-list__title profile-list__title--delivery"><?php echo $number; ?>.</div>
                <div class="profile-list__descr profile-list__descr--delivery"><?php echo $result['address']; ?></div>
                <?php if($number == 1){ ?>
                <a id="delete-link" style="cursor: pointer" class="delete-link">del</a>
                <?php }else{ ?>
                <a href="<?php echo $result['delete']; ?>" class="delete-link">del</a>
                <?php } ?>
            </li>
            <?php $number++; ?>
            <?php } ?>
        </ul>
        <a href="<?php echo $add; ?>" class="add-link">Добавить адрес</a>
    </div>
</div>
<script>
    $('#delete-link').on('click', function (e) {
        swal(
                '',
                'Вы неможете удалить основной адрес',
                'error'
        )
    })
</script>
