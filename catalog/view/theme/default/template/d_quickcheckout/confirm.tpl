<!-- 
	Ajax Quick Checkout 
	v6.0.0
	Dreamvention.com 
	d_quickcheckout/confirm.tpl 
-->
<script type="text/html" id="confirm_template" xmlns="http://www.w3.org/1999/html">
<div id="confirm_wrap">
	<div class="panel panel-default">
		<div class="panel-body" style="padding-left: 0;">
			<form id="confirm_form" class="form-horizontal">
			</form>
			<button id="qc_confirm_order" class="btn btn--order btn_btt" <%= model.show_confirm ? '' : 'disabled="disabled"' %>><?php echo $button_confirm; ?></button>

		</div>
	</div>
</div>
</script>
<script>

$(function() {
	qc.confirm = $.extend(true, {}, new qc.Confirm(<?php echo $json; ?>));
	qc.confirmView = $.extend(true, {}, new qc.ConfirmView({
		el:$("#confirm_view"), 
		model: qc.confirm, 
		template: _.template($("#confirm_template").html())
	}));
});

</script>