<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " -
    ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " -
    ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo $og_url; ?>"/>
    <?php if ($og_image) { ?>
    <meta property="og:image" content="<?php echo $og_image; ?>"/>
    <?php } else { ?>
    <meta property="og:image" content="<?php echo $logo; ?>"/>
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>"/>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>

    <script src="/catalog/view/javascript/libs/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>

    <link href="catalog/view/javascript/libs/normalize/normilize.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/theme/default/stylesheet/fonts.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/javascript/libs/magnific/magnific-popup.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/javascript/libs/slick/slick.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/javascript/libs/formstyler/jquery.formstyler.css" rel="stylesheet" type="text/css"/>

    <link href="catalog/view/theme/default/stylesheet/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/sweetalert2/6.4.2/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <script src="/catalog/view/javascript/libs/modernizr/modernizr.js" type="text/javascript"></script>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
    <link rel="stylesheet" type="text/css"
          href="catalog/view/stylesheet/pdqo/vendor/magnific-popup/magnific-popup.min.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/vendor/animate/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/pdqo.min.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/default.css"/>

    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

</head>
<body style="margin: 0;" class="<?php echo $class; ?>">

<header class="header">
    <div class="header-top clearfix">

        <div class="main-menu clearfix">
            <div class="container">
                <ul class="main-list">
                    <?php
                $colum_count = ceil($count_categories / 4);
                $count_item = 1;
                  $colum_start = '<div class="column">';
                    $colum_end = '
            </div>
            ';
            $i = 1;
            ?>
            <?php foreach ($categories as $category_1) { ?>

            <?php if($count_item == 1){
                         echo $colum_start;

                    } ?>

            <?php if($count_item == $colum_count){
                               echo $colum_end;
                               echo $colum_start;
                               $count_item = 0;
                         } ?>
            <li class="main-list__item main-list__item--title"><a
                        href="<?php echo $category_1['href'] ; ?>"><?php echo $category_1['name'] ; ?></a>
            </li>

            <?php foreach ($category_1['children'] as $category_2) { ?>
            <?php if($count_item == $colum_count){
                               echo $colum_end;
                               echo $colum_start;
                               $count_item = 0;
                         } ?>
            <li class="main-list__item"><a
                        href="<?= $category_2['href']; ?>"><?= $category_2['name']; ?></a></li>
            <?php $i ++;
            $count_item ++;
            ?>
            <?php foreach ($category_2['children'] as $category_3) { ?>
            <?php if($count_item == $colum_count){
                               echo $colum_end;
                               echo $colum_start;
                               $count_item = 0;

                         } ?>

            <li class="main-list__item main-list__item--drop"><a
                        href="<?= $category_3['href']; ?>"><?= $category_3['name']; ?></a></li>
            <?php $i ++;
            $count_item ++;
            ?>
            <?php } ?>
            <?php } ?>
            <?php if ($i == $colum_count){
                  echo $colum_end;
                ?>

            <?php } ?>
            <?php $i ++;
            $count_item ++;
            ?>
            <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container">

        <div id="nav-main" class="nav-btn">
            <div class="nav-btn__wrap">
                <b></b>
                <b></b>
                <b></b>
            </div>
            <span>Каталог</span>
        </div>
        <a href="<?php echo $home; ?>" class="logo logo--mob">
            <img class="logo__pict logo__pict--mob" src="<?php echo $logo_mob; ?>" alt="<?php echo $name; ?>">
            <div class="logo__title logo__title--mob">Novastom</div>
        </a>
        <ul class="nav-menu">
            <li class="nav-menu__item"><a href="index.php?route=information/information&information_id=4" class="nav-menu__link">О компании</a></li>
            <li class="nav-menu__item"><a href="index.php?route=information/news" class="nav-menu__link">Новости</a></li>
            <li class="nav-menu__item"><a href="index.php?route=information/information&information_id=8" class="nav-menu__link">Видео</a></li>
            <li class="nav-menu__item"><a href="index.php?route=information/information&information_id=9" class="nav-menu__link">Вакансии</a></li>
            <li class="nav-menu__item"><a href="index.php?route=information/contact" class="nav-menu__link">Контакты</a></li>
        </ul>
        <div class="contact contact--header">
            <div class="cart_adds">
                <?php if ($logged) { ?>
                <div class="personal personal-in">
                    <a href="index.php?route=account/logout" class="logout"></a>
                    <a href="index.php?route=account/account"
                       class="personal__link personal__link--signin personal__signin--in">Здравствуйте,&nbsp;<span><?php echo $firstname; ?></span></a>
                    <a href="index.php?route=checkout/checkout" class="personal__link personal__link--cart">
                        <div class="count-wrap"><span><?php echo $text_items; ?></span></div>
                    </a>
                </div>
                <a href="tel:<?= $telephone[1]; ?><?= $telephone[2]; ?>"
                   class="phone phone--header"><span>(<?= $telephone[1]; ?>)</span><?= $telephone[2]; ?></a>
                <?php }else{ ?>
                <div class="personal">
                    <a href="#login-form" class="personal__link personal__link--login personal__login--fixed popup"
                       data-effect="mfp-zoom-in"><span>Вход</span></a>
                    <a href="#signin-form" class="personal__link personal__link--signin personal__signin--fixed popup"
                       data-effect="mfp-zoom-in"><span>Регистрация</span></a>
                    <a href="index.php?route=checkout/checkout"
                       class="personal__link personal__link--cart personal__cart--fixed">
                        <div class="count-wrap"><span><?php echo $text_items; ?></span></div>
                    </a>
                </div>
                <a href="tel:<?= $telephone[1]; ?><?= $telephone[2]; ?>"
                   class="phone phone--header"><span>(<?= $telephone[1]; ?>)</span><?= $telephone[2]; ?></a>
                <?php } ?>
            </div>
        </div>


    </div>
    </div>
    <?php if(($currentRout == 'common/home')||($currentRout == '')) { ?>

    <div class="header-main clearfix">
        <div class="container">
            <a href="<?php echo $home; ?>" class="logo">
                <img class="logo__pict" src="<?php echo $logo; ?>" alt="">
                <div class="logo__title">Novastom<span class="logo__accent">5 лет успешной работы</span></div>
            </a>

            <div class="search">
                <?php echo $search; ?>
            </div>

            <a href="#present-form" class="order-present popup" data-effect="mfp-zoom-in">Заказать<span>бесплатную презентацию</span></a>

            <a href="#callback-form" class="callback callback--header popup" data-effect="mfp-zoom-in">Заказать<span>обратный звонок</span></a>
            <span><?= $cart?></span>
        </div>
    </div>
    <?php if(!empty($data['modules'][0])){ ?>
    <?php echo $data['modules'][0]; ?>
    <?php }?>
    <div class="subscribe">
        <div class="subscribe__title">Подпишитесь на наши акции:</div>
        <div class="subscribe-in">
            <input type="text" id="subscribe" name="subscribe" placeholder="Ваш e-mail">
            <button id="subscribe_h" class="subscribe-btn">subscribe<i></i></button>
        </div>
    </div>
    <?php } else { ?>
    <div class="header-main header-main--inner clearfix">
        <div class="container">
            <a href="<?php echo $home; ?>" class="logo logo-inner">
                <img class="logo__pict logo__pict--inner" src="<?php echo $logo; ?>" alt="">
                <div class="logo__title logo__title--inner">Novastom<span class="logo__accent logo__accent--inner">5 лет успешной работы</span>
                </div>
            </a>

            <div class="search search--inner">
                <?php echo $search; ?>
            </div>

            <a href="#present-form" class="order-present order-present--inner popup"
               data-effect="mfp-zoom-in">Заказать<span>бесплатную презентацию</span></a>

            <a href="#callback-form" class="callback callback--header callback--inner popup" data-effect="mfp-zoom-in">Заказать<span>обратный звонок</span></a>

            <span><?= $cart?></span>
        </div>
    </div>
    <?php } ?>
</header>

<div class="hidden">
    <div id="present-form" class="form-block form-block--modal mfp-with-anim">
        <div class="form-block__title">Заказать бесплатную презентацию товара</div>
        <form action="#" id="contactForm_c">
            <div class="form-block__in form-block__in--name"><input id="name-1" name="name" type="text"
                                                                    placeholder="Ваше имя"></div>
            <div class="form-block__in form-block__in--phone"><input id="phone-1" name="phone" type="text"
                                                                     placeholder="Ваш телефон"></div>
            <button class="btn btn--form" id="send">Заказать презентацию</button>
        </form>
    </div>

    <div id="callback-form" class="form-block form-block--modal mfp-with-anim">
        <div class="form-block__title">Заказать<br>обратный звонок</div>
        <form action="#" id="callback-form-2">
            <div class="form-block__in form-block__in--name"><input class="name-2" name="name" type="text"
                                                                    placeholder="Ваше имя"></div>
            <div class="form-block__in form-block__in--phone"><input class="phone-2" name="phone" type="text"
                                                                     placeholder="Ваш телефон"></div>
            <button class="btn btn--form" id="callback-button">Заказать звонок</button>
        </form>
    </div>
    <div id="login-form" class="modal-form mfp-with-anim">
        <div class="modal-form__title modal-form__title--login"><i></i>Войти</div>
        <form action="#" id="login">
            <input name="login" type="text" placeholder="Логин">
            <input name="password" type="password" placeholder="Пароль">
            <button id="button-login" class="btn btn--form2">Войти</button>
        </form>
    </div>

    <div id="signin-form" class="modal-form modal-form--signin mfp-with-anim">
        <div class="modal-form__title modal-form__title--signin"><i></i>Регистрация</div>
        <form action="#" id="register">

            <input type="text" id="firstname" name="firstname" placeholder="Ваше имя">
            <?php if ($error_firstname) { ?>
            <div class="text-danger"><?php echo $error_firstname; ?></div>
            <?php } ?>

            <input type="text" id="loginReg" name="loginReg" placeholder="Логин">

            <input type="password" id="password" name="password" placeholder="Пароль">


            <input type="password" id="confirm" name="confirm" placeholder="Подтверждение пароля">


            <input type="text" id="email" name="email" placeholder="E-mail">

            <button id="button_register" class="btn btn--form2">Зарегистрироваться</button>
        </form>
    </div>
</div>
<div class="loader">
    <div class="loader_inner"></div>
</div>
<script>
    $('#button-login').on('click', function (even) {
        even.preventDefault();
        var res = $('#login').serializeArray();
        var arr = {};
        $.each(res, function (result) {
            var $index = res[result].name;
            arr[$index] = res[result].value;

        });
        $.ajax({
            url: 'index.php?route=account/login/login',
            type: 'POST',
            dataType: 'json',
            data: res,
            success: function (data) {
                if (data.status == true) {
                    document.location.replace("/index.php?route=account/account");
                } else {
                    swal("Вы ввели неверные данные", "", "error");
                }
            }
        });
    });
</script>

<script>
    $('#button_register').on('click', function (even) {
        even.preventDefault();
        var res = $('#register').serializeArray();
        var arr = {};

        $.each(res, function (result) {
            var $index = res[result].name;
            arr[$index] = res[result].value;
        });
        $.ajax({
            url: 'index.php?route=account/register/register',
            type: 'post',
            dataType: 'json',
            data: res,
            success: function (data) {
                if (data.status == true) {
                    swal("Вы успешно зарегистрировались, пожалуйста выполните вход!", "", "success");
                    $.magnificPopup.close();


                    var arr = {
                        'loginReg': $('#loginReg').val(),
                        'email':$('#email').val()
                };
                        $.ajax({
                            url: 'index.php?route=common/header/registerForm',
                            type: 'post',
                            dataType: 'json',
                            data: arr,
                            success: function () {
                            }
                        });
                    $('#firstname').val('');
                    $('#loginReg').val('');
                    $('#password').val('');
                    $('#confirm').val('');
                    $('#email').val('');
                } else {
                    if(data.firstname){
                        swal("Имя должно содержать от 1 до 32 символов!", "", "error");
                    } else if(data.loginReg){
                        swal("Логин должно содержать от 1 до 32 символов!", "", "error");
                    }else if(data.password){
                        swal("В пароле должно быть от 4 до 20 символов!", "", "error");
                    }else if(data.confirm){
                        swal("Пароли не совпадают!", "", "error");
                    }else if(data.email){
                        swal("E-Mail введён неправильно!", "", "error");
                    }else if(data.warning){
                        swal("Этот E-Mail уже зарегистрирован!", "", "error");
                    }else if(data.warning2){
                        swal("Этот Логин уже зарегистрирован!", "", "error");
                    }
                }
            }
        });

    });
</script>

