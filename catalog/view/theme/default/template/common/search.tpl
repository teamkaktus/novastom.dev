<div id="search" class="input-group">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="Поиск" class="search-in" />
  <span class="input-group-btn" style="display: none">
    <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
  </span>
</div>