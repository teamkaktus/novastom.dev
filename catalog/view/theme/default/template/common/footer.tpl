<div style="clear: both"></div>
<footer class="footer clearfix">

  <div class="footer-main">
    <div class="container">

      <div class="footer-block footer-block--menu">
        <div class="footer-block__title">Меню</div>
        <ul class="footer-menu">
          <li><a href="index.php?route=information/information&information_id=4">О компании</a></li>
          <li><a href="index.php?route=information/news">Новости</a></li>
          <li><a href="index.php?route=information/information&information_id=10">Статьи</a></li>
          <li><a href="index.php?route=information/information&information_id=8">Видео</a></li>
          <li><a href="index.php?route=information/information&information_id=9">Работа</a></li>
          <li><a href="index.php?route=information/contact">Контакты</a></li>
        </ul>
      </div>

      <div class="footer-block footer-block--catalog">
        <div class="footer-block__title">Каталог</div>
        <ul class="footer-catalog">
          <?php foreach ($categories as $category) { ?>
          <li><a href="<?= $category['href']; ?>"><?= $category['name']; ?></a></li>
          <?php } ?>
        </ul>
      </div>

      <div class="footer-block footer-block--contact">

        <a href="tel:<?= $telephone[1]; ?><?= $telephone[2]; ?>" class="phone phone--footer"><span>(<?= $telephone[1]; ?>)</span><?= $telephone[2]; ?></a>

        <a href="#callback-form" class="callback callback--footer popup" data-effect="mfp-zoom-in">Заказать<span>обратный звонок</span></a>

        <div class="subscribe subscribe--footer">
          <div class="subscribe__title">Подпишитесь на наши акции</div>
          <div class="subscribe-in">
            <input type="text" id="subscribe_f" name="subscribe" placeholder="Ваш e-mail">
            <button id="subscribe_footer" class="subscribe-btn">subscribe<i></i></button>
          </div>
        </div>

      </div>

    </div>
  </div>

  <div class="footer-bottom">

    <div class="container">
      <a href="<?php echo $home; ?>" class="logo logo--footer">
        <img class="logo__pict" src="<?php echo $logo_footer; ?>" alt="">
        <div class="logo__title">Novastom<span class="logo__accent">Стоматологическое оборудование</span></div>
      </a>

      <div class="copyright">&copy; 2014 | Novastom - стоматологическое оборудование</div>

      <a href="#" class="developer">
        <span class="developer__title">Разработано в:</span>
        <img class="developer__pict" src="img/reklamoterapiya.png" alt="">
        <img class="developer__pict developer__pict--mob" src="img/reklamoterapiya-mob.png" alt="">
      </a>
    </div>

  </div>
</footer>


<!--<div class="loader">
  <div class="loader_inner"></div>
</div> -->

<!--<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p><?php echo $powered; ?></p>
  </div>
</footer> -->
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/common2.js" type="text/javascript"></script>

<script src="catalog/view/javascript/libs/html5shiv/es5-shim.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/html5shiv/html5shiv.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/html5shiv/html5shiv-printshiv.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/slick/slick.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/formstyler/jquery.formstyler.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/contactForm.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/sweetalert2/6.4.2/sweetalert2.min.js"></script>

<script type="text/javascript" src="catalog/view/javascript/pdqo/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/pdqo/vendor/numbox/jquery.numbox-1.2.0.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/pdqo/vendor/masked-input/jquery.mask.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/pdqo/jquery.pdqo-core.min.js"></script>


<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>