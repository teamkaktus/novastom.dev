<?php echo $header; ?>
<div class="container">
	<div class="breadcrumb">
		<ul class="breadcrumb-list">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
			<div class="page-title page-title--profile"><?php echo $heading_title; ?></div>
			<?php if ($news_list) { ?>
			<div class="news-page clearfix">
				<?php foreach ($news_list as $news_item) { ?>
				<div class="news-item">
						<?php if($news_item['thumb']) { ?>
						<div class="news-item__pict"><img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['title']; ?>"/></div>
						<?php }?>
						<div class="news-item__date"><span><?= $news_item['posted_day']; ?></span>&nbsp;<?= $news_item['month_ru']; ?></div>
						<a href="<?php echo $news_item['href']; ?>" class="news-item__title"><?php echo $news_item['title']; ?></a>
						<div class="news-item__descr">
							<p><?php echo $news_item['description']; ?></p>
						</div>
						<a onclick="location.href = ('<?php echo $news_item['href']; ?>');" class="btn btn--read"><span><?php echo $text_more; ?><i></i></span></a>
				</div>
				<?php } ?>
			</div>
				<div class="pagination pagination--news"><?php echo $pagination; ?></div>
			<?php } else { ?>
			<p><?php echo $text_empty; ?></p>
			<div class="buttons">
				<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
			</div>
			<?php } ?>
		<?php echo $content_bottom; ?></div>
	<?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>