<div class="slider-wrap clearfix">
    <div class="slider-nav container"></div>
    <div class="header-slider">
        <?php foreach ($banners as $banner) { ?>
        <div class="slider-item">
            <img src="<?php echo $banner['image']; ?>" alt="">
            <div class="slider-caption container">
                <div class="slider-text">
                    <span class="slider-text__top">Бесплатная</span>
                    <p>Презентация боров</p>
                    <span class="slider-text__bottom">у Вас в клинике</span>
                </div>
                <a href="<?=$banner['link']; ?>" class="btn btn--slider">Подробнее</a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
