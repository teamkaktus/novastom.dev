<div class="block clearfix">
  <div class="container">
    <div class="block__title"><?php echo $heading_title; ?></div>
    <?php foreach ($products as $product) { ?>
    <div class="block-item">
      <a href="<?php echo $product['href']; ?>" class="block-item__pict"><img src="<?php echo $product['thumb']; ?>" alt=""></a>
     <?php if (strlen($product['name'] < 45)){ ?>
      <a href="<?php echo $product['href']; ?>" class="block-item__title"><?php echo $product['name']; ?>, <?php echo $product['manufacturer']; ?></a>
<?php }else{ ?>
      <a href="<?php echo $product['href']; ?>" class="block-item__title"><?php echo $product['name']; ?>... ,<?php echo $product['manufacturer']; ?></a>
      <?php } ?>
      <div class="block-item__price"><?php echo $product['price']; ?><span>&nbsp;руб.</span></div>
      <a onclick="cart.add('<?php echo $product['product_id']; ?>')" class="block-item__cart">Корзина</a>
    </div>
    <?php } ?>
  </div>
</div>