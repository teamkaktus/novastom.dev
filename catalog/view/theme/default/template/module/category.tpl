<div class="sidebar">
    <div class="sidebar-title">Каталог</div>
    <ul class="catalog-nav">
        <?php foreach ($categories as $category_1) { ?>
        <li class="catalog-nav__item"><a href="<?php echo $category_1['href'] ; ?>"
                                         class="catalog-nav__link"><i></i><?php echo $category_1['name'] ; ?></a>
            <?php if(!empty($category_1['children'])){ ?>
            <ul class="catalog-dropnav">
                <?php foreach ($category_1['children'] as $category_2) { ?>
                <li class="catalog-dropnav__item"><a
                            href="<?= $category_2['href']; ?>"><i></i><?= $category_2['name']; ?></a>
                    <?php if(!empty($category_2['children'])){ ?>
                    <ul class="catalog-subnav">
                        <?php foreach ($category_2['children'] as $category_3) { ?>
                        <li class="catalog-subnav__item"><a
                                    href="<?= $category_3['href']; ?>"><?= $category_3['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </li>
                    <?php } ?>
            </ul>
   <?php } ?>
        </li>
            <?php } ?>
    </ul>
</div>

