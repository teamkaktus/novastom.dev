<div class="mfp-bg mfp-zoom-in mfp-ready"></div>
<div class="mfp-content">
    <div id="oneclick-form" class="modal-form mfp-with-anim" style="max-width: <?=$window['width'];?>px;">
        <div class="modal-form__title">Заказать в один клик</div>
        <form action="#">
            <label for="pdqo-n"><?php if($fields['name']['required']) { ?><i class="pdqo-required">*</i><?php } ?> </label>
            <input name="pdqo-n-field" id="pdqo-n" type="text" placeholder="Ваше имя"/>

            <label for="pdqo-p"><?php if($fields['phone']['required']) { ?><i class="pdqo-required">*</i><?php } ?> </label>
            <input name="pdqo-p-field" id="pdqo-p" type="text" placeholder="Ваш телефон"/>
        </form>
        <div class="pdqo-section pdqo-group" style="display: none">
            <div class="pdqo-col pdqo-span-12-of-12">
                <table class="pdqo-products"></table>
            </div>
        </div>
        <button class="pdqo-confirm btn btn--form2"><?=$t['checkout'];?></button>
        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
    </div>
</div>
    <?=$js;?>