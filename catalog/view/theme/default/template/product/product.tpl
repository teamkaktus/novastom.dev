<?php echo $header; ?>
<div class="content content--tovar clearfix">
<div class="container">
    <div class="breadcrumb breadcrumb--bd breadcrumb--sd">
        <ul class="breadcrumb-list">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div class="main">
        <div id="content" class="<?php echo $class; ?>">
            <div class="nav-mob">
                <div id="nav-catalog" class="nav-mob__item nav-btn nav-btn--catalog">
                    <div class="nav-btn__wrap nav-btn__wrap--catalog">
                        <b></b>
                        <b></b>
                        <b></b>
                    </div>
                    <span>Каталог</span>
                </div>
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <div class="nav-mob__item nav-mob__item--in">
                    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </div>
                <?php } ?>
                <div class="nav-mob__item nav-mob__item--end"><?php echo $heading_title; ?></div>
            </div>
            <div class="page-title page-title--catalog page-title--tovar"><?php echo $heading_title; ?></div>
            <div class="row">
                <?php if ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
                <?php } else { ?>
                <?php $class = 'col-sm-8'; ?>
                <?php } ?>
                <div class="<?php echo $class; ?>">
                    <div class="tovar clearfix">
                        <div class="tovar__pict">
                            <?php if ($thumb) { ?>
                            <ul class="thumbnails">
                                <?php if ($thumb) { ?>
                                <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img
                                                src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>"
                                                alt="<?php echo $heading_title; ?>"/></a>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </div>
                        <div class="tovar-descr">
                            <div class="tovar-descr__logo">
                                <?php if($manufacturers_img) { ?>
                                <?php echo ($manufacturers_img) ? '<img src="'.$manufacturers_img.'" title="'.$manufacturer.'" />
                                ' : '' ;?>
                                <?php } ?>
                            </div>
                            <div class="tovar-descr__text">
                                <p><?php echo $description; ?></p>
                            </div>
                            <div class="tovar-descr__price">
                                <?php if ($price) { ?>
                                <?php if (!$special) { ?>
                                <?php echo $price; ?><span>&nbsp;руб.</span>
                                <?php } else { ?>
                                <?php echo $special; ?><span>&nbsp;руб.</span>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <a onclick="cart.add('<?php echo $product_id; ?>')" class="btn btn--tovar"><i></i><?php echo $button_cart; ?></a>
                            <a class="btn btn--click" data-pdqo-item="<?php echo $product_id; ?>" onclick="pdqoObject(this);"><i></i>Купить в 1 клик</a>
                        </div>
                    </div>
                    <div class="tab-block clearfix">
                        <div class="tab-head">
                            <a href="#tab-1" class="tab-head__link active">Характеристики</a>
                            <a href="#tab-2" class="tab-head__link">Описание</a>
                            <a href="#tab-3" class="tab-head__link">Доставка и оплата</a>
                        </div>
                        <div class="tab-body">
                            <div id="tab-1" class="tab-item open">
                                <div class="tov-title"><?php echo $heading_title; ?></div>
                                <div class="tb tb-tovar">
                                    <?php if ($attribute_groups) { ?>
                                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                    <div class="tb-row">
                                        <div class="tb-cell tb-cell--title"><?php echo $attribute['name']; ?></div>
                                        <div class="tb-cell tb-cell--descr"><?php echo $attribute['text']; ?></div>
                                    </div>
                                    <?php } ?>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>

                            <div id="tab-2" class="tab-item">
                                <div class="tov-title"><?php echo $heading_title; ?></div>
                                <div class="tb">
                                    <div class="tb-row">
                                        <?php echo $description2; ?>
                                    </div>
                                </div>

                            </div>

                            <div id="tab-3" class="tab-item">
                                <div class="tov-title"><?php echo $heading_title; ?></div>
                                <div class="tb">
                                    <div class="tb-row">
                                        <?php echo $content_top; ?>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="block block-tovar clearfix">
                <?php if ($products) { ?>
                <div class="block__title">Похожие товары</div>
                <?php $i = 0; ?>
                <?php foreach ($products as $product) { ?>
                <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
                <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
                <?php } else { ?>
                <?php $class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
                <?php } ?>
                <div class="block-item block-item--sale">
                    <a href="<?php echo $product['href']; ?>" class="block-item__pict block-item__pict--sale">
                        <img src="<?php echo $product['thumb']; ?>"alt="<?php echo $product['name']; ?>"
                             title="<?php echo $product['name']; ?>"/>
                    </a>
                    <?php if (strlen($product['name'] < 35)){ ?>
                    <a href="<?php echo $product['href']; ?>" class="block-item__title"><?php echo $product['name']; ?></a>
                    <?php }else{ ?>
                    <a href="<?php echo $product['href']; ?>" class="block-item__title"><?php echo $product['name']; ?></a>
                    <?php } ?>
                    <div class="block-item__price">
                        <?php if ($product['price']) { ?>
                        <?php echo $product['price']; ?><span>&nbsp;руб.</span>
                        <?php } ?>
                    </div>
                    <a onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>')" class="block-item__cart"><?php echo $button_cart; ?></a>
                </div>
                <?php $i++; ?>
                <?php } ?>
                <?php } ?>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?>
    </div>
    </div>
</div>
</div>
<script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function() {
                $('#recurring-description').html('');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#button-cart').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-cart').button('loading');
            },
            complete: function() {
                $('#button-cart').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#cart > button').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function() {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(node).button('loading');
                    },
                    complete: function() {
                        $(node).button('reset');
                    },
                    success: function(json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function() {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function() {
                $('#button-review').button('loading');
            },
            complete: function() {
                $('#button-review').button('reset');
            },
            success: function(json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });

    $(document).ready(function() {
        $('.thumbnails').magnificPopup({
            type:'image',
            delegate: 'a',
            gallery: {
                enabled:true
            }
        });
    });
    //--></script>
<?php echo $footer; ?>