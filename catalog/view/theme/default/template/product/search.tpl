<?php echo $header; ?>
<div class="content clearfix">
<div class="container">
  <div class="breadcrumb">
    <ul class="breadcrumb-list">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> search-page clearfix"><?php echo $content_top; ?>
      <div class="search-block">
          <input type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="search-block__in" />
          <button value="<?php echo $button_search; ?>" id="button-search" class="search-block__btn">search</button>
      </div>
      <?php if ($products) { ?>
      <div class="tb tb-cart tb-search">
        <?php foreach ($products as $product) { ?>
        <div class="tb-row">
            <div class="tb-cell tb-cell--pict"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"/></a></div>
              <div class="tb-cell tb-cell--name"><?php echo $product['name']; ?><span><?php echo $product['manufacturer']; ?></span></div>
              <?php if ($product['price']) { ?>
              <div class="tb-cell tb-cell--price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>руб.
                <?php } else { ?>
                <?php echo $product['special']; ?>руб.
                <?php } ?>
              </div>
              <?php } ?>
            <div class="tb-cell tb-cell--act">
              <a href="<?php echo $product['href']; ?>" class="search-update" title="<?php echo $product['name']; ?>">update</a>
              <a onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="search-cart">cart</a>
            </div>
        </div>
        <?php } ?>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>