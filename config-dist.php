<?php
// HTTP
define('HTTP_SERVER', 'http://novastom.dev/');

// HTTPS
define('HTTPS_SERVER', 'http://novastom.dev/');

// DIR
define('DIR_APPLICATION', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/catalog/');
define('DIR_SYSTEM', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/system/');
define('DIR_LANGUAGE', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/catalog/language/');
define('DIR_TEMPLATE', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/catalog/view/theme/');
define('DIR_CONFIG', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/system/config/');
define('DIR_IMAGE', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/image/');
define('DIR_CACHE', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/system/storage/cache/');
define('DIR_DOWNLOAD', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/system/storage/download/');
define('DIR_LOGS', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/system/storage/logs/');
define('DIR_MODIFICATION', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/system/storage/modification/');
define('DIR_UPLOAD', 'D:/server_5_2_5/OpenServer/domains/novastom.dev/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'novastom');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
